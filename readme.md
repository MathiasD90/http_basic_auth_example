## HTTP Basic Authentication - demo app (Laravel 4 & AngularJS 2)
By Mathias Dewelde

## Installing
- Clone the project

- Navigate in the Server folder
```
Composer update
```
*//This will install the vendors for the server part*

## Configuring
- In the App folder navigate to app/app.js and edit the apiUrl variable