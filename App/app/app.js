﻿// ----- !!! Niet vergeten aan te passen !!! ----- //
var apiUrl = "http://localhost/Server/public/api/";
// ----------------------------------------------- //

var app = angular.module('demoApp', ['ngRoute', 'ngCookies']);

app.run(function($rootScope, $http, Base64Factory) {
    var username = "testuser";
    var password = "testpass";

    var encoded = Base64Factory.encode(username + ':' + password);

    // Door deze regel, zullen alle toekomstige $http requests een authenticatie header bevattten.
    $http.defaults.headers.common.Authorization = 'Basic ' + encoded;


    // Even testen!
    $http({method: 'GET', url: apiUrl + 'demo' }).
        success(function(data, status) {
            alert("Is authenticated: " + data.success);
        }).
        error(function() {
            alert('error');
        })
    ;

    // Eenmaal de "$http.defaults.headers.common.Authorization" is geset zal er een authenticatie header worden verzonden (Zie Firebug / Google Chrome console).
    // Ook voor requests uit services, factories, ... zal deze header meegezonden worden

});