﻿/* ROUTER */
app.config(function ($routeProvider) {
    $routeProvider
        .when('/', {
            templateUrl: 'app/partials/home.html'
        })
        .otherwise({ redirectTo: '/' })
    ;
});