<?php

/* ### API (No Authentication required) ### */
Route::group(['prefix' => 'api'], function() {
    Route::get('free', [
        'as'        => 'api.getFree', // Alias
        'uses'      => 'ApiController@getFree'
    ]);
});

/* ### API (Authentication required) ### */
Route::group(['prefix' => 'api', 'before' => 'api_auth'], function() {
    Route::get('/demo', [
        'as'        => 'api.getDemo', // Alias
        'uses'      => 'ApiController@getDemo'
    ]);
});

/* ### Api Auth ### */
Route::filter('api_auth', function(){
    if(!isset($_SERVER['PHP_AUTH_USER']) || !isset($_SERVER['PHP_AUTH_PW'])){
        return Response::json(['error' => 'Please use basic auth to provide a username and password'], 401);
    }
    if(!Auth::attempt(array('username' => $_SERVER['PHP_AUTH_USER'], 'password' => $_SERVER['PHP_AUTH_PW']))){
        return Response::json(['error' => 'The username or password was incorrect'], 401);
    }
});