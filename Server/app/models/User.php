<?php

use Illuminate\Auth\UserInterface;

class User extends Eloquent implements UserInterface {

    protected $table = 'users';
    protected $hidden = [
        'password',
        'created_at',
        'updated_at',
        'deleted_at'
    ];
    protected $guarded = ['id'];

    public function getAuthIdentifier()
    {
        return $this->getKey();
    }
    public function getAuthPassword()
    {
        return $this->password;
    }

    public static function boot()
    {
        parent::boot();
        self::creating(function($user){
            $user->password = Hash::make($user->password);
        });
    }

}