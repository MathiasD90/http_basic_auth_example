<?php

use Illuminate\Database;

class UserTableSeeder extends Seeder{

    public function run()
    {
        DB::table('users')->delete();
        User::create(array(
            'username'      => 'testuser',
            'password'      => 'testpass'
        ));
    }

}