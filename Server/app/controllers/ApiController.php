<?php

class ApiController extends Controller {

    public $restful = true;

    public function getDemo()
    {
        $data['success'] = true;
        $data['text'] = 'Authenticatie is vereist!';
        return Response::json($data);
    }

    public function getFree()
    {
        $data['success'] = true;
        $data['text'] = 'Geen authenticatie vereist';
        return Response::json($data);
    }

}